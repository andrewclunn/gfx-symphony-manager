symphony-manager
==========

A client application which provides a single interface to the symphony
management tools.

This application was developed with a modern build of the chrome browser as the
target.  Support for older versions and other browsers is not guaranteed.

## Requirements
To run this locally for building you will need to have git, node.js, and
Microsoft Visual Studios installed.

## Installing
The repo with the source is hosted at:
<!-- https://bitbucket.org/gfxinternational/AAAAAAAAAAAAA -->

First pull the repo.  After you have the source you need to run the following
command from the main optimum directory:

```
npm install gulp
npm update
```
If this is not your first time please remove the node_modules folder and run the
above commands if these commands fail.

## Building
Gulp will allow you to live update and edit code. To run gulp navigate to the
main optimum folder and run gulp by simply entering the command 'gulp'

```
gulp
```

If you're building for a server deployment, then two commands are used.  The
first clears the current build and the second recreates it, but there is no
watching for updates and auto rebuild process.

```
gulp clear
gulp deploy
```

TODO add Microsoft Visual Studios build steps
