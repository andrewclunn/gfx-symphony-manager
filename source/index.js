(function() {
	'use strict';

	angular.module('symphony-manager.sections')
		.controller('indexCtrl', ['$log', '$scope', '$rootScope', '$state',
			'$stateParams', '$location', '$auth', '$timeout', '$mdDialog', '$mdToast',
			'apiService',
			indexCtrl
		]);

	function indexCtrl($log, $scope, $rootScope, $state, $stateParams, $location,
		$auth, $timeout, $mdDialog, $mdToast, apiService) {
		// ----- DATA -----
		$rootScope.heartBeatTiming = 50000;
		$rootScope.managerSections = [
			{
				name: 'Assets',
				icon: 'web_asset'
			},
			{
				name: 'Items',
				icon: 'folder'
			},
			{
				name: 'Locations',
				icon: 'location_on'
			},
			{
				name: 'Orders',
				icon: 'insert_drive_file'
			},
			{
				name: 'Searches',
				icon: 'find_in_page'
			},
			{
				name: 'Catalogs',
				icon: 'import_contacts'
			},
			{
				name: 'Surveys',
				icon: 'poll'
			},
			{
				name: 'Asset Configuration',
				icon: 'widgets'
			},
			{
				name: 'Item Configuration',
				icon: 'create_new_folder'
			},
			{
				name: 'Location Configuration',
				icon: 'edit_location'
			},
			{
				name: 'Document Configuration',
				icon: 'description'
			},
			{
				name: 'Program Configuration',
				icon: 'work'
			}
		];
		$rootScope.currentPage = '';
		$rootScope.currentUser = {
			email: '',
			messages: {
				total: 0,
				data: []
			},
		};
		$rootScope.topMenu = '';
		$rootScope.loadingBar = {
			loading: false,
			progress: 0
		};
		$rootScope.pagination = {
			page: 1,
			limit: 25,
			total: 0,
			pageCount: 1,
			setDefaults: function() {
				$scope.pagination.page = 1;
				$scope.pagination.limit = 25;
				$scope.pagination.total = 0;
				$scope.pagination.pageCount = 1;
			},
			setPageCount: function() {
				if ($scope.pagination.total != 0) {
					$scope.pagination.pageCount =
						Math.floor($scope.pagination.total / $scope.pagination.limit);
					if (($scope.pagination.total % $scope.pagination.limit) > 0) {
						$scope.pagination.pageCount = $scope.pagination.pageCount + 1;
					}
				} else {
					$scope.pagination.pageCount = 0;
				}
			},
			onFirstPage: function() {
				if ($scope.pagination.page === 1) return true;
				return false;
			},
			onLastPage: function() {
				if ($scope.pagination.page === $scope.pagination.pageCount) return true;
				return false;
			}
		};

		// ----- FUNCTIONS -----
		// The start up function that runs on the initial page load or reload.
		function runOnControllerLoad() {
			// Run intial calls.
			$scope.heartBeat();
		}

		// Used to ensure that the present page is loaded with the correct client.
		$rootScope.setCurrentPage = function(page, companyCode) {
			$rootScope.currentPage = page || $rootScope.currentPage;
			$rootScope.companyCode = companyCode || $rootScope.companyCode || 'NONE';
			var urlCompanyCode = $location.path().split("/")[1];
			urlCompanyCode = urlCompanyCode || '';
			urlCompanyCode = urlCompanyCode.toUpperCase();
			// Prevent switching between clients using the url.
			if (($rootScope.companyCode) && (urlCompanyCode != $rootScope.companyCode)) {
				$rootScope.goToState($rootScope.currentPage, {companyCode: $rootScope.companyCode}, true);
			}
			else {
				// Gather the current user's email or log out if the authorization token
				// is not found.
				if ($auth.isAuthenticated()) {
					$rootScope.currentUser.email = $auth.getPayload().unique_name;
				}
				else {
					apiService.logout();
				}
			}
		};

		// Retrieves the user's messages on a regular interval.
		$rootScope.heartBeat = function() {
			if ($auth.isAuthenticated()) {
				apiService.gatherMessages().then(
					function(data) {
						$rootScope.currentUser.messages.total = data.total;
						$rootScope.currentUser.messages.data = data.root;
					}
				);
			}
			$timeout(function () {
					$log.log('heartBeat()');
					$scope.heartBeat();
				}, $rootScope.heartBeatTiming
			);
		}

		// Used to transition to different tools / sections of the application.
		$rootScope.goToState = function(state, params, reload) {
			params = params || {};
			reload = reload || false;
			params.companyCode = $rootScope.companyCode;
			$scope.hideMenus();
			$state.go(state, params, {reload: reload});
		};

		// Logs the user out of the application.
		$rootScope.logout = function() {
			$scope.hideMenus();
			apiService.logout();
		};

		// Returns today's date.
		$rootScope.todaysDate = function() {
			return $scope.asDate(moment());
		}

		// Converts a timestamp to the format MM/DD/YYYY
		$rootScope.asDate = function(timeStamp) {
			var timezoneOffset = new Date().getTimezoneOffset();
			return moment(timeStamp).subtract(timezoneOffset, 'minutes').format(
				'MM/DD/YYYY');
		}

		// Converts a timestamp to the format MMMM Do YYYY, h:mm:ss A
		$rootScope.asTime = function(timeStamp) {
			var timezoneOffset = new Date().getTimezoneOffset();
			return moment(timeStamp).subtract(timezoneOffset, 'minutes').format(
				'MMMM Do YYYY, h:mm:ss A');
		}

		// Closes a toast message pop-up.
		$rootScope.closeToast = function() {
			$mdToast.hide();
		}

		// Used to test create 404 resopnse from the server.
		$rootScope.generate404 = function() {
			apiService.badCall();
		}

		// Used for tied scroll bars of the data tables to keep the column headers
		// aligned with the row data.
		$rootScope.dataTableScrolled = function() {
			document.getElementById('dataTable-header').scrollLeft =
				document.getElementById('dataTable-rows').scrollLeft;
		}

		// ----- START UP -----
		runOnControllerLoad();

		// ----- TOP MENUS -----
		// Shows or hides the sections menu, depending on if it is already open.
		$scope.toggleMenu = function(menu) {
			if ($rootScope.topMenu == menu) {
				$rootScope.topMenu = '';
			}
			else {
				$rootScope.topMenu = menu;
			}
		};

		// Hides the top menus.
		$scope.hideMenus = function() {
			$rootScope.topMenu = '';
		};

		// Appends a special class to a menu if it is meant to be displayed.
		$scope.topMenuDisplay = function(menu) {
			if ($rootScope.topMenu == menu) {
				return 'show';
			}
			return '';
		};

		// Determines if an item in the sections menu should be disabled.
		$scope.sectionMenuItemDisabled = function(section) {
			if ($scope.currentPage == section.name) {
				// $log.log($scope.currentPage);
				// $log.log(section.name);
				return true;
			}
			return false;
		};

		// ----- DIALOGS -----
		// Used by the pop-up dialogs to close themselves when the user clicks the
		// 'X'.
		$rootScope.hideDialog = function() {
			$mdDialog.hide();
		}

		// Called when the dialog for viewing messages is opened.
		$scope.displayMessagesDialog = function() {
			// Set up the dialog's temporary data.
			$scope.dialogTempData = {};
			// Clears a single message.
			$scope.dialogTempData.clearMessage = function(message) {
				var messageIndex = _.findIndex($rootScope.currentUser.messages.data,
					{'Id': message.Id});
				$rootScope.currentUser.messages.data.splice(messageIndex, 1);
				$rootScope.currentUser.messages.total =
					$rootScope.currentUser.messages.data.length;
			};
			// Queries the server to update the user's messages.
			$scope.dialogTempData.refreshMessages = function() {
				apiService.gatherMessages().then(
					function(data) {
						$rootScope.currentUser.messages.total = data.total;
						$rootScope.currentUser.messages.data = data.root;
					}
				);
			};
			// Clears all messages.
			$scope.dialogTempData.clearMessages = function() {
				$log.log('clear all messages');
				$rootScope.currentUser.messages.data = [];
				$rootScope.currentUser.messages.total =
					$rootScope.currentUser.messages.data.length;
			};
			$scope.hideMenus();
			$mdDialog.show({
				scope: $scope,
				preserveScope: true,
				templateUrl: 'sections/main/messages.html',
				parent: angular.element(document.body),
				clickOutsideToClose: true
			});
		}

		// Called when the dialog for changing the password is opened.
		$scope.displayChangePasswordDialog = function() {
			// Set up the dialog's temporary data.
			$scope.dialogTempData = {};
			$scope.dialogTempData.currentPassword = '';
			$scope.dialogTempData.newPassword = '';
			$scope.dialogTempData.confirmNewPassword = '';
			$scope.hideMenus();
			$mdDialog.show({
				scope: $scope,
				preserveScope: true,
				templateUrl: 'sections/main/changePassword.html',
				parent: angular.element(document.body),
				clickOutsideToClose: true
			});
		}

		// ----- LISTENERS -----
		//
		// $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
		// 	if ($auth.isAuthenticated()) {
		// 		if (toState.name === 'login') {
		// 			$location.path('/' + toParams.companyCode + '/login');
		// 		}
		// 	}
		// 	else {
		// 		if (toState.data.authenticate) {
		// 			$location.path('/' + toParams.companyCode + '/login');
		// 		}
		// 	}
		// 	toState.data = toState.data || {};
		// 	if (toState.data.admin && (!_.includes($auth.getPayload().role, 'CanAccessAdmin'))) {
		// 		return $location.path('/' + $rootScope.companyCode + '/login');
		// 	}
		// });

		// Catches 'toastMessage' events to create a $mdToast alert message.
		$rootScope.$on('toastMessage', function(event, eventData) {
			// Used to create simple notification messages for when an api call fails.
			$mdToast.hide(); // Ensure that any existing message is cleared.
			var element = angular.element(document.getElementById('messageBar'));
			var toast = $mdToast.simple({
				template: '<md-toast>' +
					'<div class="md-toast-content ' + eventData.theme + '">' +
					'<span flex>' + eventData.message + '&nbsp;</span>' +
					'<md-icon ng-click="closeToast()">close</md-icon>' +
					'</div></md-toast>',
				position: 'top',
				hideDelay: eventData.hideDelay,
				parent: element
			});
			$mdToast.show(toast);
		});
	}
})();
