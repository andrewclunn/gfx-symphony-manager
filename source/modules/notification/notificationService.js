(function() {
	'use strict';

	angular.module('symphony-manager.modules')
		.factory('notificationService', ['$log', '$rootScope',
			notificationService
		]);

	function notificationService($log, $rootScope) {
		// Broadcasts an alert message up to the index.js to catch and display.
		function _alertMessage(message, theme, hideDelay) {
			message = message || '';
			theme = theme || 'toast-defualt';
			if (hideDelay == undefined) {
				hideDelay = 4000;
			}
			$rootScope.$broadcast('toastMessage', {
				message: message,
				theme: theme,
				hideDelay: hideDelay
			});
		}

		return {
			alertMessage: _alertMessage
		};
	}
})();
