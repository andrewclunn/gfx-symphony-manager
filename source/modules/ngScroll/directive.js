(function() {
  'use strict';

  angular.module('symphony-manager.modules')
  .directive('ngScroll', function($parse) {
    return function(scope, element, attrs) {
      var fn = $parse(attrs.ngScroll);
      element.bind('scroll', function(event) {
        scope.$apply(function() {
          fn(scope, {$event:event});
        });
      });
    };
  });
})();
