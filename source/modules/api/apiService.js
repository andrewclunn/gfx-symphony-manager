(function() {
	'use strict';

	angular.module('symphony-manager.modules')
		.factory('apiService', ['$log', '$rootScope', '$location', '$q', '$auth',
			'$timeout', 'Restangular', 'notificationService', apiService
		]);

	function apiService($log, $rootScope, $location, $q, $auth, $timeout,
	Restangular, notificationService) {

		// Handles the get calls.
		function _get(path, paramters) {
			return Restangular.all(path).customGET(paramters)
		}

		function _clearURL() {
			locationSearch = $location.search();
			for (var parameter in locationSearch) {
				$location.search(parameter, null);
			}
		}

		// Get url parameters.
		function _getURL() {
			var queryParameters = $location.search();
			return angular.copy(queryParameters);
		}

		// Adds / edits the url query parameters.
		function _updateURL(queryParameters) {
			var locationSearch = $location.search();
			// Clears unvalued location parameters.
			for (var locationParameter in locationSearch) {
				if (!queryParameters[locationParameter]) {
					$location.search(locationParameter, null);
				}
			}
			// Updates valued location parameters.
			for (var queryParameter in queryParameters) {
				$location.search(queryParameter, queryParameters[queryParameter].toString());
			}
		}

		// Formats the query parameters for a get call.
		function _formatQueryParameters(queryParameters) {
			// Builds the callArguments list.
			var callArguments = [];
			var finalQueryParameters = '';
			if (queryParameters) {
				for (var parameter in queryParameters) {
					if (queryParameters[parameter]) {
						callArguments.push(parameter + '=' + queryParameters[parameter].toString());
					}
				}
				// Formats the final list for the url.
				if (callArguments.length > 0) {
					finalQueryParameters = callArguments.join('&');
					finalQueryParameters = '?' + finalQueryParameters;
				}
			}
			return finalQueryParameters;
		}

		// Formats the query parameters for a get and performs the get call.  Also
		// updates the url with the query parameters.
		function _find(url, queryParameters) {
			var finalQueryParameters = _formatQueryParameters(queryParameters);
			return _get(url, finalQueryParameters);
		}

		// Similar to the _find call, but also updates the url with the query
		// parameters.
		function _search(url, queryParameters) {
			_updateURL(queryParameters);
			return _find(url, queryParameters);
		}

		//
		function _gatherMessages() {
			return Restangular.all('Notification/Messages').customGET();
		}

		//
		function _getLocationCharacteristics() {
			return Restangular.all('Composer/Characteristic/ListByTemplateJson').customGET();
		}

		//
		function _badCall() {
			return Restangular.all('fakeCall/ThatDoesNotExist').customGET();
		}

		// Used to maek a fake call for testing purposes.
		function _fakeDataCall() {
			var deferred = $q.defer();
			var fakeData = {
				total: 4,
				root: [
					{
						Id: "f0a99844-ee27-4266-9753-819f620960dc",
						Time: "9/15/2016 9:58 AM",
						Message: "Your download is ready: \r\n\r\n\u003ca href=\"/checkers/Notification/Download?notificationId=f0a99844-ee27-4266-9753-819f620960dc\u0026amp;fileName=ItemCharacteristics9-15-2016.xlsx\"\u003eItemCharacteristics9-15-2016.xlsx\u003c/a\u003e"
					},
					{
						Id: "abc123",
						Time: "9/15/2016 10:38 PM",
						Message: "I am a message."
					},
					{
						Id: "xyz456",
						Time: "9/16/2016 3:00 AM",
						Message: "I am another message."
					},
					{
						Id: "msg789",
						Time: "9/18/2016 12:21 AM",
						Message: "I am pretending to be a message."
					}
				]
			};
			$timeout(function() {
				deferred.resolve(fakeData);
			}, 300);
			return deferred.promise;
		}

		//
		function _fakeDataCall2() {
			var deferred = $q.defer();
			var fakeData = {
				total: 8,
				root: [
					{
						"Code": "6e712b21-4065-4e8f-b429-14d7af479b72",
						"Name": "AddressType",
						"Description": "",
						"DefaultValue": "Restaurant",
						"Formula": "N/A",
						"ValidationListCount": "Restaurant, Residential, Office",
						"TypeOfCharacteristic": "Text",
						"CurrentState": "FixedList"
					},
					{
						"Code": "4f4679fc-9c01-4038-bcba-1067680ca1fe",
						"Name": "Contact Name",
						"Description": "Contact Name for ordering POP",
						"DefaultValue": "Please enter Name",
						"Formula": "N/A",
						"ValidationListCount": "N/A",
						"TypeOfCharacteristic": "Text",
						"CurrentState": "Standard"
					},
					{
						"Code": "bd16109c-b1d0-4297-81b9-d5f4e8a3984d",
						"Name": "ContactPreference",
						"Description": "",
						"DefaultValue": "0",
						"Formula": "N/A",
						"ValidationListCount": "N/A",
						"TypeOfCharacteristic": "Text",
						"CurrentState": "Standard"
					},
					{
						"Code": "ade6a26d-f37e-487a-bc59-c84f26404edc",
						"Name": "Corporate DM",
						"Description": "Current Corporate District Manager",
						"DefaultValue": "False",
						"Formula": "N/A",
						"ValidationListCount": "False, True",
						"TypeOfCharacteristic": "Boolean",
						"CurrentState": "FixedList"
					},
					{
						"Code": "AAAAAAAAAA",
						"Name": "AAAAAAAAAA",
						"Description": "AAAAAAAAAA",
						"DefaultValue": "True",
						"Formula": "N/A",
						"ValidationListCount": "False, True",
						"TypeOfCharacteristic": "Boolean",
						"CurrentState": "FixedList"
					},
					{
						"Code": "BBBBBBBBBB",
						"Name": "BBBBBBBBBB",
						"Description": "BBBBBBBBBB",
						"DefaultValue": "False",
						"Formula": "N/A",
						"ValidationListCount": "False, True",
						"TypeOfCharacteristic": "Boolean",
						"CurrentState": "FixedList"
					},
					{
						"Code": "XYXYXYXYXY",
						"Name": "XYXYXYXYXY",
						"Description": "XYXYXYXYXY",
						"DefaultValue": "True",
						"Formula": "N/A",
						"ValidationListCount": "False, True",
						"TypeOfCharacteristic": "Boolean",
						"CurrentState": "FixedList"
					},
					{
						"Code": "3333333333",
						"Name": "3333333333",
						"Description": "3333333333",
						"DefaultValue": "False",
						"Formula": "N/A",
						"ValidationListCount": "False, True",
						"TypeOfCharacteristic": "Boolean",
						"CurrentState": "FixedList"
					}
				]
			};
			$timeout(function() {
				deferred.resolve(fakeData);
			}, 300);
			return deferred.promise;
		}

		//
		function _fakeDataCall3() {
			var deferred = $q.defer();
			var fakeData = {
				total: 4,
				root: [
					{
						"id": "73397819-1d59-46fb-b329-a9601d719084",
						"text": "Office Location",
						"leaf": false,
						"leafTotal": 15,
						"recursiveLeafTotal": 15,
						"type": null
					},
					{
						"id": "f8b63722-d471-4500-aeee-33c576de1add",
						"text": "Restaurant Template",
						"leaf": false,
						"leafTotal": 191,
						"recursiveLeafTotal": 191,
						"type": null
					},
					{
						"id": "73397819-d471-46fb-b329-a9601d719084",
						"text": "Street Cart",
						"leaf": false,
						"leafTotal": 2,
						"recursiveLeafTotal": 2,
						"type": null
					},
					{
						"id": "f8b63722-d471-4500-aeee-a9601d719084",
						"text": "Mother's Kitchen",
						"leaf": false,
						"leafTotal": 8,
						"recursiveLeafTotal": 8,
						"type": null
					}
				]
			};
			$timeout(function() {
				deferred.resolve(fakeData);
			}, 300);
			return deferred.promise;
		}

		//
		function _login(email, password) {
	    $auth.login({
				email: email,
				password: password,
				grant_type: 'password'
      })
      .then(function () {
				$rootScope.goToState('Symphony Manager', {companyCode: $rootScope.companyCode}, true);
      	}
			)
			.catch(function (rejection) {
				notificationService.alertMessage(
					'Username / Password is not valid.', 'toast-warning');
      });
		}

		// Clear the cookie and send the user to the Login page.
		function _logout() {
			$auth.logout();
			if ($rootScope.currentPage != 'Login') {
				$rootScope.goToState('Login', {companyCode: $rootScope.companyCode}, true);
			}
		}

		return {
			getURL: _getURL,
			updateURL: _updateURL,
			formatQueryParameters: _formatQueryParameters,
			find: _find,
			search: _search,
			gatherMessages: _fakeDataCall,
			getLocationCharacteristics: _fakeDataCall2,
			getLocationCharacteristicTemplates: _fakeDataCall3,
			login: _login,
			logout: _logout,
			badCall: _badCall
		};
	}
})();
