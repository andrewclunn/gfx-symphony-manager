(function() {
	'use strict';

	angular.module('symphony-manager', [
			'ui.router',
			'ct.ui.router.extras.core',
			'ct.ui.router.extras.dsr',
			'ct.ui.router.extras.sticky',
			'satellizer',
			'ngAnimate',
			'ngMaterial',
			'restangular',
			'templates',
			'symphony-manager.modules',
			'symphony-manager.sections'
		])
		.config(_appConfig)
		.factory('authInterceptor', _authInterceptor);

	// Main application configuration.
	function _appConfig($stateProvider, $urlRouterProvider, $httpProvider,
		$authProvider, $logProvider, $stickyStateProvider, RestangularProvider) {
		// Logging
		$logProvider.debugEnabled(true);

		// Debug sticky states
		$stickyStateProvider.enableDebug(false);

		// HTTP interceptors
		$httpProvider.interceptors.push('authInterceptor');
		$httpProvider.defaults.headers.patch = {
			'Content-Type': 'application/json;charset=utf-8',
		};

		window.apiBase = 'https://test-a.gfxsymphony.com';
		window.apiVersion = '/v1';
		window.apiOAuth = window.apiBase + window.apiVersion + '/oauth2/token';

		// Set base url for all api calls.
    $authProvider.loginUrl = window.apiOAuth;
    $authProvider.tokenName = 'access_token';
    RestangularProvider.setBaseUrl(window.apiBase + window.apiVersion);
    RestangularProvider.setDefaultHeaders({
        'Content-Type': 'application/json'
    });
		// var apiBase = 'https://test-a.gfxsymphony.com';
		// var apiBase = 'https://test-a.gfxsymphony.com' + '/v1';
    // RestangularProvider.setBaseUrl(apiBase);

		// Sets the default state to the login page, which will log the user out if
		// they navigate or are sent there.
		$urlRouterProvider.otherwise('/:companyCode/login');
	}

	// Captures server responses, some of which require further actions.
	function _authInterceptor($q, $rootScope, $location, $injector) {
		var companyCode = $location.path().split("/")[1];
		$rootScope.companyCode = companyCode || '';
		$rootScope.companyCode = $rootScope.companyCode.toUpperCase();
		if ($rootScope.companyCode == ':COMPANYCODE') {
			$rootScope.companyCode = 'NONE';
		}
		return {
			request: function(config) {
				$rootScope.loadingBar.loading = true; // You are loading.
				config.headers['x-company-code'] = $rootScope.companyCode;
        if (config.data && config.data.grant_type) {
          config.data = $.param({
            username: config.data.email,
            password: config.data.password,
            grant_type: 'password'
          });
          config.headers['Content-Type'] = 'application/x-www-form-urlencoded';
        } else {
        	config.headers = config.headers || {};
        }
				return config || $q.when(config);
			},
			response: function(response) {
				$rootScope.loadingBar.loading = false; // You are done loading.
				return response || $q.when(response);
			},
			responseError: function(rejection) {
				$rootScope.loadingBar.loading = false; // You are done loading.
				switch (rejection.status) {
					case 0:
						$rootScope.$broadcast('toastMessage', {
							message: 'Error: Could not connect to server.',
							theme: 'toast-error',
							hideDelay: 0
						});
						break;
						// Log the user out.
					case 401: // Unauthorized
					case 403: // Forbidden
						$injector.get('apiService').logout();
						break;
					// Not found.
					case 404: // Not Found
					case -1: // Server not found or available.
						$rootScope.$broadcast('toastMessage', {
							message: 'The server does not recognize / support this call.',
							theme: 'toast-error',
							hideDelay: 0
						});
						break;
						// Show message to user for certain call failures
					case 400: // Bad request
					case 405: // Method not allowed
					case 409: // Conflict
					case 500: // Server error
						if ((rejection.data) && (rejection.data.message)) {
							$rootScope.$broadcast('toastMessage', {
								message: rejection.data.message,
								theme: 'toast-error',
								hideDelay: 0
							});
						} else {
							$rootScope.$broadcast('toastMessage', {
								message: 'Error code: ' + rejection.status,
								theme: 'toast-error',
								hideDelay: 0
							});
						}
						break;
					default:
						$rootScope.$broadcast('toastMessage', {
							message: 'Error code: ' + rejection.status,
							theme: 'toast-error'
						});
						break;
				}
				return $q.reject(rejection);
			}
		};
	}
})();
