(function() {
  'use strict';

  angular.module('symphony-manager.sections')
    .config(['$stateProvider', routeFunctionCatalogs]);

  function routeFunctionCatalogs($stateProvider) {
    var stateParams = {
      url: '/:companyCode/catalogs',
      views: {
        'content@': {
          templateUrl: 'sections/catalogs/template.html',
          controller: 'catalogsCtrl'
        }
      },
      params: {
        page: 'Catalogs'
      }
    };

    $stateProvider.state('Catalogs', stateParams);
  }
})();
