(function() {
	'use strict';

	angular.module('symphony-manager.sections')
		.controller('catalogsCtrl', ['$log', '$scope', '$state', '$stateParams',
			'apiService', 'notificationService',
			catalogsCtrl
		]);

	function catalogsCtrl($log, $scope, $state, $stateParams, apiService,
		notificationService) {
		// ----- DATA -----
		$scope.localData = {};

		// ----- FUNCTIONS -----
		// The start up function that runs on the initial page load or reload.
		function runOnControllerLoad() {
			// Set default values.
			$scope.setCurrentPage('Catalogs');
		}

		// ----- START UP -----
		runOnControllerLoad();
	}
})();
