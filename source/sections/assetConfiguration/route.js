(function() {
  'use strict';

  angular.module('symphony-manager.sections')
    .config(['$stateProvider', routeFunctionAssetConfiguration]);

  function routeFunctionAssetConfiguration($stateProvider) {
    var stateParams = {
      url: '/:companyCode/assetConfiguration',
      views: {
        'content@': {
          templateUrl: 'sections/assetConfiguration/template.html',
          controller: 'assetConfigurationCtrl'
        }
      },
      params: {
        page: 'Asset Configuration'
      }
    };

    $stateProvider.state('Asset Configuration', stateParams);
  }
})();
