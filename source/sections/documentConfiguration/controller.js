(function() {
	'use strict';

	angular.module('symphony-manager.sections')
		.controller('documentConfigurationCtrl', ['$log', '$scope', '$state',
			'$stateParams', 'apiService', 'notificationService',
			documentConfigurationCtrl
		]);

	function documentConfigurationCtrl($log, $scope, $state, $stateParams,
		apiService, notificationService) {
		// ----- DATA -----
		$scope.localData = {};

		// ----- FUNCTIONS -----
		// The start up function that runs on the initial page load or reload.
		function runOnControllerLoad() {
			// Set default values.
			$scope.setCurrentPage('Document Configuration');
		}

		// ----- START UP -----
		runOnControllerLoad();
	}
})();
