(function() {
  'use strict';

  angular.module('symphony-manager.sections')
    .config(['$stateProvider', routeFunctionDocumentConfiguration]);

  function routeFunctionDocumentConfiguration($stateProvider) {
    var stateParams = {
      url: '/:companyCode/documentConfiguration',
      views: {
        'content@': {
          templateUrl: 'sections/documentConfiguration/template.html',
          controller: 'documentConfigurationCtrl'
        }
      },
      params: {
        page: 'Document Configuration'
      }
    };

    $stateProvider.state('Document Configuration', stateParams);
  }
})();
