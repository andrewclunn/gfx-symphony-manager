(function() {
  'use strict';

  angular.module('symphony-manager.sections')
    .config(['$stateProvider', routeFunctionItemConfiguration]);

  function routeFunctionItemConfiguration($stateProvider) {
    var stateParams = {
      url: '/:companyCode/itemConfiguration',
      views: {
        'content@': {
          templateUrl: 'sections/itemConfiguration/template.html',
          controller: 'itemConfigurationCtrl'
        }
      },
      params: {
        page: 'Item Configuration'
      }
    };

    $stateProvider.state('Item Configuration', stateParams);
  }
})();
