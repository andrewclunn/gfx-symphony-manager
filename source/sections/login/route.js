(function() {
  'use strict';

  angular.module('symphony-manager.sections')
    .config(['$stateProvider', routeFunctionLogin]);

  function routeFunctionLogin($stateProvider) {
    var stateParams = {
      url: '/:companyCode/login',
      views: {
        'content@': {
          templateUrl: 'sections/login/template.html',
          controller: 'loginCtrl'
        }
      },
      params: {
        page: 'Login'
      }
    };

    $stateProvider.state('Login', stateParams);
  }
})();
