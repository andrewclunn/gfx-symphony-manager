(function() {
	'use strict';

	angular.module('symphony-manager.sections')
		.controller('loginCtrl', ['$log', '$scope', '$state', '$stateParams',
			'apiService', 'notificationService',
			loginCtrl
		]);

	function loginCtrl($log, $scope, $state, $stateParams, apiService,
		notificationService) {
		// ----- DATA -----
		$scope.localData = {
			user: '',
			password: '',
			clientList: [
				{
					Key: 'NONE',
					Display: 'Select your Client'
				},
				{
					Key: 'SPG',
					Display: 'Sprint'
				},
				{
					Key: 'CHECKERS',
					Display: 'Checkers'
				}
			]
		};

		// ----- FUNCTIONS -----
		// The start up function that runs on the initial page load or reload.
		function runOnControllerLoad() {
			// Set default values.
			$scope.setCurrentPage('Login');
		}

		// Logs the user in to the application.
		$scope.login = function() {
			if ($scope.companyCode == 'NONE') {
				notificationService.alertMessage(
					'No client selected.', 'toast-warning');
			}
			else {
				apiService.login($scope.localData.user, $scope.localData.password);
			}
		};

		// ----- START UP -----
		runOnControllerLoad();
	}
})();
