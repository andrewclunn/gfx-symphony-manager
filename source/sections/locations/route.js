(function() {
  'use strict';

  angular.module('symphony-manager.sections')
    .config(['$stateProvider', routeFunctionLocations]);

  function routeFunctionLocations($stateProvider) {
    var stateParams = {
      url: '/:companyCode/locations',
      views: {
        'content@': {
          templateUrl: 'sections/locations/template.html',
          controller: 'locationsCtrl'
        }
      },
      params: {
        page: 'Locations'
      }
    };

    $stateProvider.state('Locations', stateParams);
  }
})();
