(function() {
	'use strict';

	angular.module('symphony-manager.sections')
		.controller('locationsCtrl', ['$log', '$scope', '$state', '$stateParams',
			'apiService', 'notificationService',
			locationsCtrl
		]);

	function locationsCtrl($log, $scope, $state, $stateParams, apiService,
		notificationService) {
		// ----- DATA -----
		$scope.localData = {};

		// ----- FUNCTIONS -----
		// The start up function that runs on the initial page load or reload.
		function runOnControllerLoad() {
			// Set default values.
			$scope.setCurrentPage('Locations');
		}

		// ----- START UP -----
		runOnControllerLoad();
	}
})();
