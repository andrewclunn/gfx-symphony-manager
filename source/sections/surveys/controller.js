(function() {
	'use strict';

	angular.module('symphony-manager.sections')
		.controller('surveysCtrl', ['$log', '$scope', '$state', '$stateParams',
			'apiService', 'notificationService',
			surveysCtrl
		]);

	function surveysCtrl($log, $scope, $state, $stateParams, apiService,
		notificationService) {
		// ----- DATA -----
		$scope.localData = {};

		// ----- FUNCTIONS -----
		// The start up function that runs on the initial page load or reload.
		function runOnControllerLoad() {
			// Set default values.
			$scope.setCurrentPage('Surveys');
		}

		// ----- START UP -----
		runOnControllerLoad();
	}
})();
