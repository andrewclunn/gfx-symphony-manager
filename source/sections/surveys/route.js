(function() {
  'use strict';

  angular.module('symphony-manager.sections')
    .config(['$stateProvider', routeFunctionSurveys]);

  function routeFunctionSurveys($stateProvider) {
    var stateParams = {
      url: '/:companyCode/surveys',
      views: {
        'content@': {
          templateUrl: 'sections/surveys/template.html',
          controller: 'surveysCtrl'
        }
      },
      params: {
        page: 'Surveys'
      }
    };

    $stateProvider.state('Surveys', stateParams);
  }
})();
