(function() {
  'use strict';

  angular.module('symphony-manager.sections')
    .config(['$stateProvider', routeFunctionAssets]);

  function routeFunctionAssets($stateProvider) {
    var stateParams = {
      url: '/:companyCode/assets',
      views: {
        'content@': {
          templateUrl: 'sections/assets/template.html',
          controller: 'assetsCtrl'
        }
      },
      params: {
        page: 'Assets'
      }
    };

    $stateProvider.state('Assets', stateParams);
  }
})();
