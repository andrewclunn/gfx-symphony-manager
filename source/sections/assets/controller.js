(function() {
	'use strict';

	angular.module('symphony-manager.sections')
		.controller('assetsCtrl', ['$log', '$scope', '$state', '$stateParams',
			'apiService', 'notificationService',
			assetsCtrl
		]);

	function assetsCtrl($log, $scope, $state, $stateParams, apiService,
		notificationService) {
		// ----- DATA -----
		$scope.localData = {};

		// ----- FUNCTIONS -----
		// The start up function that runs on the initial page load or reload.
		function runOnControllerLoad() {
			// Set default values.
			$scope.setCurrentPage('Assets');
		}

		// ----- START UP -----
		runOnControllerLoad();
	}
})();
