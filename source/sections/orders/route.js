(function() {
  'use strict';

  angular.module('symphony-manager.sections')
    .config(['$stateProvider', routeFunctionOrders]);

  function routeFunctionOrders($stateProvider) {
    var stateParams = {
      url: '/:companyCode/orders',
      views: {
        'content@': {
          templateUrl: 'sections/orders/template.html',
          controller: 'ordersCtrl'
        }
      },
      params: {
        page: 'Orders'
      }
    };

    $stateProvider.state('Orders', stateParams);
  }
})();
