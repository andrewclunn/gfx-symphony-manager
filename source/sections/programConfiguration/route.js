(function() {
  'use strict';

  angular.module('symphony-manager.sections')
    .config(['$stateProvider', routeFunctionProgramConfiguration]);

  function routeFunctionProgramConfiguration($stateProvider) {
    var stateParams = {
      url: '/:companyCode/programConfiguration',
      views: {
        'content@': {
          templateUrl: 'sections/programConfiguration/template.html',
          controller: 'programConfigurationCtrl'
        }
      },
      params: {
        page: 'Program Configuration'
      }
    };

    $stateProvider.state('Program Configuration', stateParams);
  }
})();
