(function() {
	'use strict';

	angular.module('symphony-manager.sections')
		.controller('programConfigurationCtrl', ['$log', '$scope', '$state',
			'$stateParams', 'apiService', 'notificationService',
			programConfigurationCtrl
		]);

	function programConfigurationCtrl($log, $scope, $state, $stateParams,
		apiService, notificationService) {
		// ----- DATA -----
		$scope.localData = {};

		// ----- FUNCTIONS -----
		// The start up function that runs on the initial page load or reload.
		function runOnControllerLoad() {
			// Set default values.
			$scope.setCurrentPage('Program Configuration');
		}

		// ----- START UP -----
		runOnControllerLoad();
	}
})();
