(function() {
  'use strict';

  angular.module('symphony-manager.sections')
    .config(['$stateProvider', routeFunctionImport]);

  function routeFunctionImport($stateProvider) {
    var stateParams = {
      url: '/:companyCode/admin',
      views: {
        'content@': {
          templateUrl: 'sections/admin/template.html',
          controller: 'adminCtrl'
        }
      },
      params: {
        page: 'Admin'
      }
    };

    $stateProvider.state('admin', stateParams);
  }
})();
