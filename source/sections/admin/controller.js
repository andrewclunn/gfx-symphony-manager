(function() {
	'use strict';

	angular.module('symphony-manager.sections')
		.controller('adminCtrl', ['$log', '$scope', '$state', '$mdDialog',
			'apiService', 'notificationService',
			adminCtrl
		]);

	function adminCtrl($log, $scope, $state, $mdDialog, apiService,
		notificationService) {
		// ----- DATA -----
		$scope.localData = {
			userList: {
				total: -1,
				data: []
			},
			roles: [{
				Key: 'Admin',
				enabled: false
			}, {
				Key: 'Client',
				enabled: false
			}, {
				Key: 'SystemAdmin',
				enabled: false
			}],
			clientList: {
				total: -1,
				data: []
			},
			selectedUser: {},
			dialogTempData: {}
		};

		// ----- FUNCTIONS -----
		// The start up function that runs on the initial page load or reload.
		function runOnControllerLoad() {
			// Set default values.
			$scope.localData.userList.total = -1;
			$scope.localData.userList.data = [];
			$scope.localData.clientList.total = -1;
			$scope.localData.clientList.data = [];
			// Get the client list.
			$scope.getClientListPage(1);
			// Gather the user list.
			$scope.getUserListPage(1);
		}

		// Gets the next page of data for the client list.
		$scope.getClientListPage = function(page) {
			apiService.search('Admin/Client/ListJson', {
				page: page,
				limit: 100
			}).then(function(data) {
				$scope.localData.clientList.total = data.results;
				$scope.localData.clientList.data =
					$scope.localData.clientList.data.concat(data.root);
				if ($scope.localData.clientList.total > $scope.localData.clientList.data
					.length) {
					$scope.getClientListPage(page + 1);
				}
			});
		}

		// Gets the next page of data for the users list.
		$scope.getUserListPage = function(page) {
			apiService.search('Admin/User/ListJson', {
				page: page,
				limit: 100
			}).then(function(data) {
				$scope.localData.userList.total = data.results;
				$scope.localData.userList.data =
					$scope.localData.userList.data.concat(data.root);
				if ($scope.localData.userList.total > $scope.localData.userList.data.length) {
					$scope.getUserListPage(page + 1);
				}
			});
		}

		// Gathers all the needed values to edit a user.
		$scope.editUser = function(user) {
			$scope.dialogTempData = {};
			$scope.dialogTempData.UserName = user.UserName || user.email || null;
			if (!$scope.dialogTempData.UserName) return false;
			// Populate the user's role list.
			$scope.dialogTempData.roles = [];
			for (var i = 0; i < $scope.localData.roles.length; i++) {
				$scope.dialogTempData.roles.push({
					Key: $scope.localData.roles[i].Key,
					enabled: false
				});
			}
			for (var i = 0; i < user.Roles.length; i++) {
				var tempIndex =
					_.findIndex($scope.dialogTempData.roles, {
						'Key': user.Roles[i]
					});
				if (tempIndex >= 0) {
					$scope.dialogTempData.roles[tempIndex].enabled = true;
				}
			}
			// Populate the user's client list.
			$scope.dialogTempData.clients = [];
			for (var i = 0; i < $scope.localData.clientList.total; i++) {
				$scope.dialogTempData.clients.push({
					Key: $scope.localData.clientList.data[i].ClientName,
					enabled: false
				});
			}
			for (var i = 0; i < user.Clients.length; i++) {
				var tempIndex =
					_.findIndex($scope.dialogTempData.clients, {
						'Key': user.Clients[i]
					});
				if (tempIndex >= 0) {
					$scope.dialogTempData.clients[tempIndex].enabled = true;
				}
			}
			// Launch the dialog.
			$scope.displayEditUserDialog();
		}

		// Adds or removes a client to a user.
		$scope.toggleClient = function(client, UserName) {
			if (client.enabled) {
				apiService.addClientToUser(client.Key, UserName).then(
					function(data) {
						if (UserName == $scope.currentUser.email) {
							$scope.whoAmI();
						}
					}
				);
			} else {
				apiService.removeClientFromUser(client.Key, UserName).then(
					function(data) {
						if (UserName == $scope.currentUser.email) {
							$scope.whoAmI();
						}
					}
				);
			}
		}

		// Adds or removes a role to a user.
		$scope.toggleRole = function(role, UserName) {
			if (role.enabled) {
				apiService.addRoleToUser(role.Key, UserName).then(
					function(data) {
						if (UserName == $scope.currentUser.email) {
							$scope.whoAmI();
						}
					}
				);
			} else {
				apiService.removeRoleFromUser(role.Key, UserName).then(
					function(data) {
						if (UserName == $scope.currentUser.email) {
							$scope.whoAmI();
						}
					}
				);
			}
		}

		// ----- START UP -----
		runOnControllerLoad();

		// ----- DIALOGS -----
		// Used by the pop-up dialogs to close themselves when the user clicks the
		// 'X'.
		$scope.hide = function() {
			$mdDialog.hide();
		}

		// Deletes a user.
		$scope.delete = function() {
			// You can't delete yourself.
			if ($scope.dialogTempData.user.UserName != $scope.currentUser.email) {
				// If the UserId is 0, then the user only exists in active directory.
				if ($scope.dialogTempData.user.UserId > 0) {
					apiService.deleteUser($scope.dialogTempData.user.UserId).then(function() {
						$scope.localData.userList.total = -1;
						$scope.localData.userList.data = [];
						$mdDialog.hide();
						$scope.getUserListPage(1);
					});
				} else {
					notificationService.alertMessage(
						'An active directory user cannot be deleted.', 'toast-warning');
					$mdDialog.hide();
				}
			} else {
				notificationService.alertMessage('You cannot delete yourself.',
					'toast-warning');
				$mdDialog.hide();
			}
		}

		// Adds a new user based on the dialog's inputs.
		$scope.addUser = function() {
			apiService.createUser($scope.dialogTempData.userName,
				$scope.dialogTempData.password, $scope.dialogTempData.vendorId).then(
				function() {
					$scope.localData.userList.total = -1;
					$scope.localData.userList.data = [];
					$mdDialog.hide();
					$scope.getUserListPage(1);
				});
		}

		// Called when the dialog for confirming the deletion of a user is opened.
		$scope.confirmDelete = function(user) {
			$scope.dialogTempData = {};
			$scope.dialogTempData.user = user;
			var userName = user.UserName || user.email;
			$scope.dialogTempData.deleteMessages = ['Delete user ' + userName];
			$mdDialog.show({
				scope: $scope,
				preserveScope: true,
				templateUrl: 'modules/admin/delete.html',
				parent: angular.element(document.body),
				clickOutsideToClose: true
			});
		}

		// Called when the dialog for editting a user is opened.
		$scope.displayEditUserDialog = function() {
			$mdDialog.show({
				scope: $scope,
				preserveScope: true,
				templateUrl: 'modules/admin/editUser.html',
				parent: angular.element(document.body),
				clickOutsideToClose: true
			});
		}

		// Called when the dialog for adding a user is opened.
		$scope.displayAddUserDialog = function() {
			$scope.dialogTempData = {};
			$mdDialog.show({
				scope: $scope,
				preserveScope: true,
				templateUrl: 'modules/admin/addUser.html',
				parent: angular.element(document.body),
				clickOutsideToClose: true
			});
		}
	}
})();
