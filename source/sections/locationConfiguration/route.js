(function() {
  'use strict';

  angular.module('symphony-manager.sections')
    .config(['$stateProvider', routeFunctionLocationConfiguration]);

  function routeFunctionLocationConfiguration($stateProvider) {
    var stateParams = {
      url: '/:companyCode/locationConfiguration',
      views: {
        'content@': {
          templateUrl: 'sections/locationConfiguration/template.html',
          controller: 'locationConfigurationCtrl'
        }
      },
      params: {
        page: 'Location Configuration'
      }
    };

    $stateProvider.state('Location Configuration', stateParams);
  }
})();
