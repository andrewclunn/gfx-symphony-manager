(function() {
	'use strict';

	angular.module('symphony-manager.sections')
		.controller('locationConfigurationCtrl', ['$log', '$scope', '$state',
			'$stateParams', '$mdDialog', 'apiService', 'notificationService',
			locationConfigurationCtrl
		]);

	function locationConfigurationCtrl($log, $scope, $state, $stateParams,
		$mdDialog, apiService, notificationService) {
		// ----- DATA -----
		$scope.localData = {
			tempval: {},
			templates: {
				current: {
					id: '',
					text: 'All Templates'
				},
				total: 0,
				data: []
			},
			characteristics: {
				total: 0,
				data: []
			}
		};

		// ----- FUNCTIONS -----
		// The start up function that runs on the initial page load or reload.
		function runOnControllerLoad() {
			// Set default values.
			$scope.setCurrentPage('Location Configuration');
			$scope.setTemplate();
		}

		$scope.testFunc = function() {
			$log.log($scope.localData.templates.current);
			$log.log($scope.localData.tempval);
		}

		// Sets the current location characteristic template.
		$scope.setTemplate = function(template) {
			// Set the current template.
			if (template) {
				$scope.localData.templates.current = {
					id: template.id,
					text: template.text
				};
			}
			// Clear existing data.
			$scope.localData.templates.total = 0;
			$scope.localData.templates.data = [];
			// Gather the child templates of the current template.
			var params = {};
			params.id = $scope.localData.templates.current.id;
			apiService.getLocationCharacteristicTemplates(params)
			.then(function (data) {
					$scope.localData.templates.total = data.total;
					$scope.localData.templates.data = data.root;
					$scope.localData.templates.data.unshift($scope.localData.templates.current);
					$scope.gatherCharacteristics();
      	}
			);
		}

		// Gather the characteristics within the current template.
		$scope.gatherCharacteristics = function() {
			// Clear existing data.
			$scope.localData.characteristics.total = 0;
			$scope.localData.characteristics.data = [];
			// Gather the child templates of the current template.
			var params = {};
			params.id = $scope.localData.templates.current.id;
			apiService.getLocationCharacteristics(params)
			.then(function (data) {
				$scope.localData.characteristics.total = Number(data.total);
				$scope.localData.characteristics.data = data.root;
				$scope.pagination.total = $scope.localData.characteristics.total;
				$scope.pagination.setPageCount();
		    }
			);
		}

		// ----- START UP -----
		runOnControllerLoad();

		// ----- DIALOGS -----
		// Called when the dialog for changing an item's image is opened.
		$scope.displayImageDialog = function(item) {
			// Set up the dialog's temporary data.
			$scope.dialogTempData = {};
			$scope.dialogTempData.name = item.Name;
			$scope.dialogTempData.image = item.Code || '';
			$scope.dialogTempData.file = '';
			$scope.hideMenus();
			$mdDialog.show({
				scope: $scope,
				preserveScope: true,
				templateUrl: 'sections/locationConfiguration/changeImage.html',
				parent: angular.element(document.body),
				clickOutsideToClose: true
			});
		}

		// Called when the options dialog for an item is opened.
		$scope.displayOptionsDialog = function(item) {
			// Set up the dialog's temporary data.
			$scope.dialogTempData = {};
			$scope.hideMenus();
			$mdDialog.show({
				scope: $scope,
				preserveScope: true,
				templateUrl: 'sections/locationConfiguration/copy.html',
				parent: angular.element(document.body),
				clickOutsideToClose: true
			});
		}

		// Called when the dialog for unlinking an item from a grouping is opened.
		$scope.displayUnlinkDialog = function(item) {
			// Set up the dialog's temporary data.
			$scope.dialogTempData = {};
			$scope.dialogTempData.messages = [
				'This will unlink ' + item.Name + ' from the ' + 'AAAA' + ' grouping.'
			];
			$scope.dialogTempData.confirm = {
				text: 'Unlink',
				icon: 'link',
				action: function() {
					$scope.hideDialog();
					alert('UNLINKED');
				}
			};
			$scope.hideMenus();
			$mdDialog.show({
				scope: $scope,
				preserveScope: true,
				templateUrl: 'sections/main/confirmation.html',
				parent: angular.element(document.body),
				clickOutsideToClose: true
			});
		}

		// Called when the dialog for copying an item is opened.
		$scope.displayCopyDialog = function(item) {
			// Set up the dialog's temporary data.
			$scope.dialogTempData = {};
			$scope.hideMenus();
			$mdDialog.show({
				scope: $scope,
				preserveScope: true,
				templateUrl: 'sections/locationConfiguration/copy.html',
				parent: angular.element(document.body),
				clickOutsideToClose: true
			});
		}

		// Called when the dialog for deleting an item is opened.
		$scope.displayDeleteDialog = function(item) {
			// Set up the dialog's temporary data.
			$scope.dialogTempData = {};
			$scope.dialogTempData.messages = [
				'This will delete the ' + item.Name + ' location configuration.'
			];
			$scope.dialogTempData.confirm = {
				text: 'Delete',
				icon: 'delete_forever',
				action: function() {
					$scope.hideDialog();
					alert('DELETED');
				}
			};
			$scope.hideMenus();
			$mdDialog.show({
				scope: $scope,
				preserveScope: true,
				templateUrl: 'sections/main/confirmation.html',
				parent: angular.element(document.body),
				clickOutsideToClose: true
			});
		}
	}
})();
