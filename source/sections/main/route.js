(function() {
  'use strict';

  angular.module('symphony-manager.sections')
    .config(['$stateProvider', routeFunctionMain]);

  function routeFunctionMain($stateProvider) {
    var stateParams = {
      url: '/:companyCode/',
      views: {
        'content@': {
          templateUrl: 'sections/main/template.html',
          controller: 'mainCtrl'
        }
      },
      params: {
        page: 'Symphony Manager'
      }
    };

    $stateProvider.state('Symphony Manager', stateParams);
  }
})();
