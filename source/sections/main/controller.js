(function() {
	'use strict';

	angular.module('symphony-manager.sections')
		.controller('mainCtrl', ['$log', '$scope', '$state', '$stateParams',
			'apiService', 'notificationService',
			mainCtrl
		]);

	function mainCtrl($log, $scope, $state, $stateParams, apiService,
		notificationService) {
		// ----- DATA -----
		$scope.localData = {};

		// ----- FUNCTIONS -----
		// The start up function that runs on the initial page load or reload.
		function runOnControllerLoad() {
			// Set default values.
			$scope.setCurrentPage('Symphony Manager');
		}

		// ----- START UP -----
		runOnControllerLoad();
	}
})();
