(function() {
  'use strict';

  angular.module('symphony-manager.sections')
    .config(['$stateProvider', routeFunctionSearches]);

  function routeFunctionSearches($stateProvider) {
    var stateParams = {
      url: '/:companyCode/searches',
      views: {
        'content@': {
          templateUrl: 'sections/searches/template.html',
          controller: 'searchesCtrl'
        }
      },
      params: {
        page: 'Searches'
      }
    };

    $stateProvider.state('Searches', stateParams);
  }
})();
