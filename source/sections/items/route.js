(function() {
  'use strict';

  angular.module('symphony-manager.sections')
    .config(['$stateProvider', routeFunctionItems]);

  function routeFunctionItems($stateProvider) {
    var stateParams = {
      url: '/:companyCode/items',
      views: {
        'content@': {
          templateUrl: 'sections/items/template.html',
          controller: 'itemsCtrl'
        }
      },
      params: {
        page: 'Items'
      }
    };

    $stateProvider.state('Items', stateParams);
  }
})();
