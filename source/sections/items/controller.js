(function() {
	'use strict';

	angular.module('symphony-manager.sections')
		.controller('itemsCtrl', ['$log', '$scope', '$state', '$stateParams',
			'apiService', 'notificationService',
			itemsCtrl
		]);

	function itemsCtrl($log, $scope, $state, $stateParams, apiService,
		notificationService) {
		// ----- DATA -----
		$scope.localData = {};

		// ----- FUNCTIONS -----
		// The start up function that runs on the initial page load or reload.
		function runOnControllerLoad() {
			// Set default values.
			$scope.setCurrentPage('Items');
		}

		// ----- START UP -----
		runOnControllerLoad();
	}
})();
