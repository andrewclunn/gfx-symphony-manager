// This is used to load the applications' components.
angular.module('templates', []);
angular.module('symphony-manager.modules', []);
angular.module('symphony-manager.sections', []);
