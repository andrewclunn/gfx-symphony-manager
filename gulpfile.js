var gulp = require('gulp');
var packageJson = require('./package.json');

/************************************* Variables ******************************/
var plugins = {};
var gulpBuild = {
  folders: {}
};
gulpBuild.folders.source = 'source/';
gulpBuild.folders.build = 'build/';
gulpBuild.folders.other = {
  source: gulpBuild.folders.source + 'other/' + '**/*',
  build: gulpBuild.folders.build + 'other/'
};
gulpBuild.folders.fonts = {
  source: gulpBuild.folders.source + 'fonts/' + '**/*.{ttf,woff,woff2,eof,svg}',
  build: gulpBuild.folders.build + 'fonts/'
};
gulpBuild.folders.images = {
  source: gulpBuild.folders.source + 'images/' + '**/*.{png,jpg,gif}',
  build: gulpBuild.folders.build + 'images/'
};
gulpBuild.folders.css = {
  source: gulpBuild.folders.source + '**/*.css',
  build: gulpBuild.folders.build + 'css/',
  index: gulpBuild.folders.source + 'index.css'
};
gulpBuild.folders.js = {
  build: gulpBuild.folders.build + 'js/'
};
gulpBuild.folders.templates = {
  source: [
    gulpBuild.folders.source + '**/*.html',
    '!' + gulpBuild.folders.source + 'index.html'
  ],
  build: gulpBuild.folders.js.build
};
gulpBuild.folders.libs = {
  // Unlike other includes, the order matters here, so the files are specified.
  source: [
    gulpBuild.folders.source + 'libs/angular-js-*.js',
    gulpBuild.folders.source + 'libs/jquery-*.js',
    gulpBuild.folders.source + 'libs/lodash-*.js',
    gulpBuild.folders.source + 'libs/moment-*.js',
    gulpBuild.folders.source + 'libs/restangular-*.js',
    gulpBuild.folders.source + 'libs/angular-ui-router-*.js',
    gulpBuild.folders.source + 'libs/ct-ui-router-extras.core-*.js',
    gulpBuild.folders.source + 'libs/ct-ui-router-extras.dsr-*.js',
    gulpBuild.folders.source + 'libs/ct-ui-router-extras.sticky-*.js',
    gulpBuild.folders.source + 'libs/satellizer-*.js',
    gulpBuild.folders.source + 'libs/angular-aria-*.js',
    gulpBuild.folders.source + 'libs/angular-animate-*.js',
    gulpBuild.folders.source + 'libs/angular-material-*.js'
  ],
  build: gulpBuild.folders.js.build
};
gulpBuild.folders.modules = {
  source: [
    gulpBuild.folders.source + 'bootstrap.js',
    gulpBuild.folders.source + 'modules/**/*.js',
    gulpBuild.folders.source + 'sections/**/*.js',
    gulpBuild.folders.source + 'index.js',
    gulpBuild.folders.source + 'app.js',
    '!' + gulpBuild.folders.source + 'libs/*.js'
  ],
  build: gulpBuild.folders.js.build
};
gulpBuild.folders.index = {
  source: gulpBuild.folders.source + 'index.html',
  build: gulpBuild.folders.build
};

/************************************* Plugins ********************************/
// Load plugins on the fly by looking at the package.json file, which prevents
// the need to install development modules on production servers.
plugins.sequence = require('run-sequence').use(gulp);
(Object.keys(packageJson.dependencies)).forEach(function(module) {
  // Ignore gulp and run-sequence (already loaded) and bin hidden directory.
  // and jsdoc (loaded with ui doc task)
  if (['gulp', 'run-sequence', '.bin'].indexOf(module) === -1) {
  // if (['gulp', 'run-sequence', 'timers-ext', '.bin'].indexOf(module) === -1) {
    // Plugins named by substring after last '-' to keep the names short and
    // prevent annoying linter errors.
    plugins[module.split('-').pop()] = require(module);
  }
});

/************************************* Sub-Tasks ******************************/
// clean:other - Clears the other files from the build directory.
gulp.task('clean:other', function() {
  return plugins.del(gulpBuild.folders.other.build);
});

// build:other - Copies other files from the source to the build directory.
gulp.task('build:other', function() {
  return gulp.src(gulpBuild.folders.other.source)
    .pipe(gulp.dest(gulpBuild.folders.other.build));
});

// clean:images - Clears the images files from the build directory.
gulp.task('clean:images', function() {
  return plugins.del(gulpBuild.folders.images.build);
});

// build:images - Copies images files from the source to the build directory.
gulp.task('build:images', function() {
  return gulp.src(gulpBuild.folders.images.source)
    .pipe(gulp.dest(gulpBuild.folders.images.build));
});

// clean:fonts - Clears the font files from the build directory.
gulp.task('clean:fonts', function() {
  return plugins.del(gulpBuild.folders.fonts.build);
});

// build:fonts - Copies font files from the source to the build directory.
gulp.task('build:fonts', function() {
  return gulp.src(gulpBuild.folders.fonts.source)
    .pipe(gulp.dest(gulpBuild.folders.fonts.build));
});

// clean:css - Clears the css files from the build directory.
gulp.task('clean:css', function() {
  return plugins.del(gulpBuild.folders.css.build);
});

// build:css - Concatenates css files from the source to the build directory.
gulp.task('build:css', function() {
  return gulp.src(gulpBuild.folders.css.source)
    .pipe(plugins.concat('styles.css'))
    // .pipe(plugins.css()) // Minification.
    .pipe(plugins.rev()) // Ensures a new revision number is appended.
    .pipe(gulp.dest(gulpBuild.folders.css.build));
});

// clean:js - Clears the javascript files from the build directory.
gulp.task('clean:js', function() {
  return plugins.del(gulpBuild.folders.js.build);
});

// clean:templates - Clears the javascript template file from the build directory.
gulp.task('clean:templates', function() {
  return plugins.del(gulpBuild.folders.js.build + 'templates-*.js');
});

// build:templates - Copies the html files from the source to the build directory.
gulp.task('build:templates', function() {
  return gulp.src(gulpBuild.folders.templates.source)
    // .pipe(plugins.html()) // Minification
    .pipe(plugins.templatecache())
    // .pipe(plugins.uglify()) // Minification after conversion to javascript.
    .pipe(plugins.rev()) // Ensures a new revision number is appended.
    .pipe(gulp.dest(gulpBuild.folders.templates.build));
});

// clean:libs - Clears the javascript libs file from the build directory.
gulp.task('clean:libs', function() {
  return plugins.del(gulpBuild.folders.js.build + 'libs-*.js');
});

// build:libs - Concatenates the javascript library files from the source to the
// build directory.
gulp.task('build:libs', function() {
  return gulp.src(gulpBuild.folders.libs.source)
    .pipe(plugins.concat('libs.js'))
    // .pipe(plugins.uglify()) // Minification.
    .pipe(plugins.rev()) // Ensures a new revision number is appended.
    .pipe(gulp.dest(gulpBuild.folders.libs.build));
});

// clean:modules - Clears the javascript modules file from the build directory.
gulp.task('clean:modules', function() {
  return plugins.del(gulpBuild.folders.js.build + 'modules-*.js');
});

// build:modules - Concatenates the javascript files for the modules from the
// source to the build directory.
gulp.task('build:modules', function() {
  return gulp.src(gulpBuild.folders.modules.source)
    .pipe(plugins.concat('modules.js'))
    // .pipe(plugins.uglify()) // Minification.
    .pipe(plugins.rev()) // Ensures a new revision number is appended.
    .pipe(gulp.dest(gulpBuild.folders.modules.build));
});

// clean:index - Clears the index.html file from the build directory.
gulp.task('clean:index', function() {
  return plugins.del(gulpBuild.folders.index.build + 'index.html');
});

// build:index - Copies and injects generated files into index.html.
gulp.task('build:index', function() {
  var jsOrder = [
    'js/libs-*.js',
    'js/modules-*.js',
    'js/templates-*.js'
  ];
  return gulp.src(gulpBuild.folders.index.source)
    .pipe(plugins.inject(gulp.src(jsOrder, {
      read: false,
      cwd: gulpBuild.folders.index.build,
    }), {
      addRootSlash: false,
    }))
    .pipe(plugins.inject(gulp.src('css/*.css', {
      read: false,
      cwd: gulpBuild.folders.index.build,
    }), {
      addRootSlash: false,
    }))
    // .pipe(plugins.html()) // Minification.
    .pipe(gulp.dest(gulpBuild.folders.index.build));
});

/************************************* Main Tasks *****************************/
// clean - Deletes gerenerated files from the build folder.
gulp.task('clean', function() {
  var cleanConcurrent = [
    'clean:other',
    'clean:images',
    'clean:fonts',
    'clean:css',
    'clean:js',
    'clean:index'
  ];
  plugins.sequence(cleanConcurrent);
  // return plugins.del(gulpBuild.folders.build);
});

// build - Builds everything required for deployment and development.
gulp.task('build', ['clean'], function(cb) {
  var buildConcurrent = [
    'build:other',
    'build:images',
    'build:fonts',
    'build:css',
    'build:libs',
    'build:modules',
    'build:templates'
  ];
  plugins.sequence(buildConcurrent, 'build:index', cb);
});

// fileWatcher - Looks for changes to the files in the source directory.  On a
// change the corresponding build files are cleared and rebuilt and the index is
// rebuilt if the resulting files are injected at build (those with revision
// numbers).
gulp.task('fileWatcher', function() {
  // Watch other files.
  gulp.watch(gulpBuild.folders.other.source, {interval: 300}, function() {
    plugins.sequence('clean:other', 'build:other');
  });

  // Watch images.
  gulp.watch(gulpBuild.folders.images.source, {interval: 300}, function() {
    plugins.sequence('clean:images', 'build:images');
  });

  // Watch fonts.
  gulp.watch(gulpBuild.folders.fonts.source, {interval: 300}, function() {
    plugins.sequence('clean:fonts', 'build:fonts');
  });

  // Watch css.
  gulp.watch(gulpBuild.folders.css.source, {interval: 300}, function() {
    plugins.sequence('clean:css', 'build:css', 'build:index');
  });

  // Watch templates.
  gulp.watch(gulpBuild.folders.templates.source, {interval: 300}, function() {
    plugins.sequence('clean:templates', 'build:templates', 'build:index');
  });

  // Watch libraries.
  gulp.watch(gulpBuild.folders.libs.source, {interval: 300}, function() {
    plugins.sequence('clean:libs', 'build:libs', 'build:index');
  });

  // Watch modules.
  gulp.watch(gulpBuild.folders.modules.source, {interval: 300}, function() {
    plugins.sequence('clean:modules', 'build:modules', 'build:index');
  });

  // Watch index.
  gulp.watch(gulpBuild.folders.index.source, {interval: 300}, function() {
    plugins.sequence('clean:index', 'build:index');
  });
});

// webserver - Builds everything required for deployment and development.
gulp.task('webserver', function() {
  plugins.webserver = require('gulp-webserver');
  return gulp.src(gulpBuild.folders.build)
    .pipe(plugins.webserver({
      livereload: true,
      fallback: 'index.html',
      directoryListing: false,
      open: true
    }));
});

/******************************** Command-line Tasks **************************/

// Local development build that watches for changes and reloads as needed while
// serving from a local web server.
gulp.task('default', function() {
  plugins.sequence('build', 'fileWatcher', 'webserver');
});

// Cleans the files for clean deployment.
gulp.task('clear', function() {
  plugins.sequence('clean');
});

// Builds the files for deployment.
gulp.task('deploy', function() {
  plugins.sequence('build');
});
